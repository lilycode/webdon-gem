# For CodeKit (from https://github.com/thoughtbot/bourbon/blob/master/lib/bourbon.rb)
dir = File.dirname(__FILE__)
$LOAD_PATH.unshift dir unless $LOAD_PATH.include?(dir)

require "webdon/version"

unless defined?(Sass)
  require 'sass'
end
unless defined?(Bourbon)
  require 'bourbon'
end
unless defined?(Neat)
  require 'neat'
end

module Webdon
  if defined?(Rails) && defined?(Rails::Engine)
    class Engine < ::Rails::Engine
      require 'webdon/engine'
    end

    module Rails
      class Railtie < ::Rails::Railtie
        rake_tasks do
          load "tasks/install.rake"
        end
      end
    end
  else
    Sass.load_paths << File.expand_path("../../app/assets/stylesheets", __FILE__)
  end
end
