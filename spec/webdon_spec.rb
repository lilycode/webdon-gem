require 'webdon'


describe Webdon::Sass do
  it "has a defined version" do
    expect(Webdon::Sass::VERSION).to_not be(nil)
  end
  
  it "Loads Sass unless loaded" do
    expect(defined?(Sass)).to_not be(nil)
  end
  
  it "loads Bourbon unless loaded" do
    puts "#{Sass.load_paths}"
    expect(defined?(Bourbon)).to_not be(nil)
  end
  
  it "loads Neat unless loaded" do
    puts "#{Sass.load_paths}"
    expect(defined?(Neat)).to_not be(nil)
  end
end