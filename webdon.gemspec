# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'webdon/sass/version'

Gem::Specification.new do |spec|
  spec.name          = "webdon"
  spec.version       = Webdon::VERSION
  spec.authors       = ["Jan Lindblom"]
  spec.email         = ["janlindblom@fastmail.fm"]
  spec.summary       = %q{Short summary. Required.}
  spec.description   = %q{Longer description. Optional.}
  spec.homepage      = "https://bitbucket.org/lilycode/webdon-gem"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "sass", "~> 3.4"
  spec.add_dependency "compass", "~> 1.0"
  spec.add_dependency "bourbon", "~> 4.0"
  spec.add_dependency "neat", "~> 1.6"
  spec.add_dependency "bitters", "~> 0.10"
  
end
